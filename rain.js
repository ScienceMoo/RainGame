var canvas
var context

// constants for game play
var rainAngle
var rainDensity
var TIME_INTERVAL = 25; // screen refresh interval in milliseconds
var canvasWidth; // width of the canvas
var canvasHeight; // height of the canvas

// person
var person // will hold speed and whether the person has an umbrella or not

// the rain
var NUM_DROPS
var raindrops

// variables for the game loop and tracking statistics
var intervalTimer // holds interval timer

// variables for sounds
var rainSound


// called when the app first launches
function setupGame()
{
	// get the canvas, its context and setup its click event handler
	canvas = document.getElementById( "theCanvas" )
	context = canvas.getContext("2d")

	raindrops = []
	person = {} // speed, hasUmbrella

	rainSound = document.getElementById( "rainSound" );

} // end function setupGame


function startTimer()
{
   intervalTimer = window.setInterval( updatePositions, TIME_INTERVAL );
} // end function startTimer

function stopTimer()
{
   canvas.removeEventListener( "click", fireCannonball, false );
   window.clearInterval( intervalTimer );
} // end function stopTimer


function resetElements() {
	var w = canvas.width;
	var h = canvas.height;
	canvasWidth = w; // store the width
	canvasHeight = h; // store the height
}

