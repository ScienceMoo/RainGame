# RainGame
Do you get wetter walking or running in the rain? This has been disputed for many years. Myth busters did an episode on it. A guy in 1972 used math to prove that running is better to minimize wetness, as long as the rain is falling vertically. What better way to test this than with a visual simulation? Let's go!
